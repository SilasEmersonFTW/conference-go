from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests


def get_photo(query):
    url = f"https://api.pexels.com/v1/search?query={query}"

    headers = {
        "Authorization": PEXELS_API_KEY,
    }

    response = requests.get(url, headers=headers)
    api_dict = response.json()
    picture = {
        "picture_url": api_dict["photos"][0]["src"]["original"]
        }
    return picture


def get_weather_data(city, state):
    # Create the URL for the geocoding API with the city and state
    url = "http://api.openweathermap.org/geo/1.0/direct"
    params = {
        "q": f"{city}, {state}, US",
        "limit": 1,
        "appid": OPEN_WEATHER_API_KEY,
    }
    response = requests.get(url, params=params)

    # Parse the JSON response
    api_dict = response.json()
    # Get the latitude and longitude from the response
    try:
        latitude = api_dict[0]["lat"]
        longitude = api_dict[0]["lon"]
    except(KeyError, IndexError):
        return None
    params = {
        "lat": latitude,
        "lon": longitude,
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperal",
    }
    url = "https://api.openweathermap.org/data/2.5/weather"
    response = requests.get(url, params=params)
    content = response.json()
    return {
        "description": content["weather"][0]["description"],
        "temperature": content["main"]["temp"],
    }
